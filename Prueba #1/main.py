from menu import opciones
from cambio import conversion

if __name__ == '__main__':

 print("---------Bienvenido a Mi conversor Digital---------\n"
       "------------Hecho por Jonathan gallardo------------\n")

 op = opciones.menu()
 print(op)

 if op == "1":
   kgrs = float(input("Introduzca Kilogramos: "))
   cmb = conversion.kilo_a_libra(kgrs)
   print("Son:", cmb, "Libras.")
 elif op == "2":
   lbs = float(input("Introduzca Libras: "))
   cmb = conversion.libra_a_kilo(lbs)
   print("Son: ", cmb, "kilogramos.")

 print("\n---------Gracias por Usarnos---------\n"
       "---------Que tengas Bello Día---------1")